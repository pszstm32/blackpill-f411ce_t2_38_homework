# Board  
[More](https://docs.zephyrproject.org/latest/boards/arm/blackpill_f411ce/doc/index.html)  
[Schematic](https://github.com/WeActTC/MiniSTM32F4x1/tree/master/HDK)  
# Connection  
Leds connection common cathode  
KEY [Button] - PA0  
PC13 [LED] - PC13  
  
KEY_EX1 [Button] - PB9  
LD3 - PA1  
LD4 - PA2  
LD5 - PA3  
LD6 - PA4  
